'use strict'

import {Card} from "./card.js";

const node = document.querySelector('.feed');

async function getContent() {

    let resp = await fetch('https://ajax.test-danit.com/api/json/posts')
    let posts = await resp.json()
    resp = await fetch('https://ajax.test-danit.com/api/json/users')
    let users = await resp.json()

    await posts.forEach(post => {
        users.forEach(user => {
            user.id === post.userId ? new Card(post, user).render(node) : false;
        })
    })

}

getContent()