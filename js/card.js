'use strict'



export class Card {
    constructor(obj, user) {

        const {body, title, id} = obj;
        const {name, email} = user;

        this.id = id;
        this.body = body;
        this.title = title;
        this.authorFullName = name;
        this.authorEmail = email;

    }

    request(method, urlSuffix = '') {
        let url = 'https://ajax.test-danit.com/api/json/posts' + urlSuffix;
        let options = {
            method: `${method}`,
            headers: {
                'Content-type': 'application/json'
            },
            body: ''
        }

        return fetch(url, options)
    }

    getPopUp() {
        let popUp = document.createElement('div');
        popUp.classList.add('popup');

        document.body.append(popUp)
    }

    async edit(objId, post) {

        this.getPopUp();

        let result = await this.request('PUT', `/${objId}`)
        if (result.ok) {
            console.log('success')
        } else {
            throw new Error('Something goes wrong...' + result.body)
        }
    }

    async delete(objId, post) {

        let result = await this.request('DELETE', `/${objId}`)
        if (result.ok) {
            post.remove()
        } else {
            throw new Error('Something goes wrong...' + result.body)
        }

    }

    render(node) {
        const htmlEl = document.createElement('div')
        htmlEl.classList.add('post')
        htmlEl.innerHTML = `
                            <span>${this.authorFullName}</span>
                            <a href="$mailto:{this.authorEmail}">${this.authorEmail}</a>
                            <h3>${this.title}</h3>
                            <p>${this.body}</p>
                            `

        let edit = document.createElement('button')
        let del = document.createElement('button')
        edit.type = 'button'
        del.type = 'button'

        edit.innerText = 'Edit twit'
        edit.classList.add('btn')

        del.innerText = 'Delete twit'
        del.classList.add('btn')

        edit.addEventListener('click', ev => { this.edit(this.id, ev.target.parentElement) })
        del.addEventListener('click', ev => { this.delete(this.id, ev.target.parentElement) })

        htmlEl.append(edit, del)

        node.append(htmlEl);

    }

}

